module Zuse(clk, IDEX_MemRe, IDEX_Rt, IFID_Rt, IFID_Rs, Bubble,
           ZeroMUX_out, IFID_Wr, PC_MX, IFID_clr);
        input clk;
        input IDEX_MemRe;
        input [4:0] IDEX_Rt;
        input [4:0] IFID_Rt;
        input [4:0] IFID_Rs;
        input Bubble;
        //input Branch;
        //input Branch_out;
        //input jump;
       // input Bubble;
        
        output reg ZeroMUX_out;
        output reg IFID_Wr;
        output reg PC_MX;
        output reg IFID_clr;
        
        always@(Bubble or IDEX_MemRe or IDEX_Rt or IFID_Rs or IFID_Rt ) 
        begin
        if(Bubble)
          begin
            ZeroMUX_out = 0;
            IFID_Wr = 0;
            PC_MX = 1;
            IFID_clr = 1;
          end
        else if (IDEX_MemRe && ((IDEX_Rt == IFID_Rs) || (IDEX_Rt == IFID_Rt)))
        begin
            ZeroMUX_out = 1;
            IFID_Wr = 0;
            PC_MX = 1;
            IFID_clr = 0;
        end
        else 
            begin
            ZeroMUX_out = 0;
            IFID_Wr = 1;
            PC_MX = 0; 
            IFID_clr = 0;
            end
        end
endmodule
