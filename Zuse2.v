
module Zuse2( Bubble, origin_bubble,
           ZeroMUX_out, IFID_Wr, PC_MX);

      
        input Bubble;
        input origin_bubble;
        
        output reg ZeroMUX_out;
        output reg IFID_Wr;
        output reg PC_MX;
        
        always@(Bubble or origin_bubble) 
        begin
        if(Bubble || origin_bubble)
          begin
            ZeroMUX_out = 1;
            IFID_Wr = 0;
            PC_MX = 1;
          end
        else 
            begin
            ZeroMUX_out = 0;
            IFID_Wr = 1;
            PC_MX = 0; 
            end
        end
endmodule

