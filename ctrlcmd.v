`include "instruction_def.v"
`include "ctrl_encode_def.v"
module ctrlcmd(OpCode,funct,
              ExtOp, ctrl_out );
	
	input [5:0]		OpCode;				//???????
	input [5:0]		funct;				//??????

	reg  jump;	
	output reg [1:0] ExtOp;
	
	output reg [12:0] ctrl_out; 
						
	 reg  RegDst;						 
	 reg  Branch;						//? ? 
	 reg  MemR;						//? ???
	 reg  Mem2R;						//???? ??????
	 reg  MemW;						//?? ????
	 reg  RegW;						//????? ???
	 reg  Alusrc;						//?? ??????
	 reg[4:0] AluOp;						//A lu????
	
	
	//assign jump = 1;
	//assign RegDst = OpCode[0];
	//assign Branch = !(OpCode[0]||OpCode[1])&&OpCode[2];
	//assign MemR = (OpCode[0]&&OpCode[1]&&OpCode[5])&&(!OpCode[3]);
	//assign Mem2R = MemR;
	//assign MemW = OpCode[1]&&OpCode[0]&&OpCode[3]&&OpCode[5];
	//assign RegW = (OpCode[2]&&OpCode[3])||(!OpCode[2]&&!OpCode[3]);
	//assign Alusrc = OpCode[0]||OpCode[1];
	//assign ExtOp = OpCode[2]&&OpCode[3];
	
	always@(OpCode or funct)
	begin
	  case ( OpCode )
	    `INSTR_RTYPE_OP: 
	    begin
	      assign RegDst = 1;
	      assign Alusrc = 0;
	      assign Mem2R = 1;//???
	      assign RegW = 1;
	      assign MemR = 0;
	      assign MemW = 0;
	      assign Branch = 0;
	      assign jump = 0;
	      assign ExtOp = `EXT_ZERO;
	       case (funct)
	         6'b100001:  
	           assign AluOp = `ALUOp_ADDU;
	         6'b100011:  
	           assign AluOp = `ALUOp_SUBU;
	         default: ;
	       endcase
	    end
	    `INSTR_ORI_OP:
	    begin
	      assign RegDst = 0;
	      assign Alusrc = 1;
	      assign Mem2R = 1;
	      assign RegW = 1;
	      assign MemR = 0;
	      assign MemW = 0;
	      assign Branch = 0;
	      assign jump = 0;
	      assign ExtOp = `EXT_SIGNED;
	      assign AluOp = `ALUOp_OR;
	    end
	    `INSTR_LW_OP:
	    begin
	      assign RegDst = 0;
	      assign Alusrc = 1;
	      assign Mem2R = 0;
	      assign RegW = 1;
	      assign MemR = 1;
	      assign MemW = 0;
	      assign Branch = 0;
	      assign jump = 0;
	      assign ExtOp = `EXT_SIGNED;
	      assign AluOp = `ALUOp_ADDU;
	    end
	    `INSTR_SW_OP:
	    begin
	      assign RegDst = 0;
	      assign Alusrc = 1;
	      assign Mem2R = 0;
	      assign RegW = 0;
	      assign MemR = 0;
	      assign MemW = 1;
	      assign Branch = 0;
	      assign jump = 0;
	      assign ExtOp = `EXT_SIGNED;
	      assign AluOp = `ALUOp_ADDU;
	    end
	     `INSTR_BEQ_OP:
	    begin
	      assign RegDst = 0;
	      assign Alusrc = 0;
	      assign Mem2R = 0;
	      assign RegW = 0;
	      assign MemR = 0;
	      assign MemW = 0;
	      assign Branch = 1;
	      assign jump = 0;
	      assign ExtOp = `EXT_SIGNED;
	      assign AluOp = `ALUOp_SUBU;
	    end
	    `INSTR_J_OP:
	    begin
	      assign RegDst = 0;
	      assign Alusrc = 0;
	      assign Mem2R = 0;
	      assign RegW = 0;
	      assign MemR = 0;
	      assign MemW = 0;
	      assign Branch = 0;
	      assign jump = 1;
	      assign ExtOp = `EXT_SIGNED;
	      assign AluOp = `ALUOp_SUBU;
	    end
	    default:  ;
	  endcase
	 ctrl_out = {jump, RegW, Mem2R, Branch, MemR, MemW, RegDst, AluOp, Alusrc};
	end
	
endmodule