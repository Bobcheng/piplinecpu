module PC( clk, rst, PCWr, J, MX, NPC, PC);
           
   input         clk;
   input         rst;
   input         PCWr;
   input         J;
   input         MX;
   input  [31:0] NPC;
   
   output reg[31:0] PC;
   
   integer i;               
   always @(posedge clk or posedge rst) 
   begin
      if ( rst ) 
         PC <= 32'h0000_3000;   
         
      PC = PC + 4;
      if ( PCWr ) 
         begin
				PC = NPC  ;
         end
				 
		else if (J)
			begin
			    PC = {PC[31:28],NPC[25:0],2'd0};
			end
      else if ( MX )
        PC = PC -4;
   end // end always
           
endmodule
