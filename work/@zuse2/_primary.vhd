library verilog;
use verilog.vl_types.all;
entity Zuse2 is
    port(
        Bubble          : in     vl_logic;
        origin_bubble   : in     vl_logic;
        ZeroMUX_out     : out    vl_logic;
        IFID_Wr         : out    vl_logic;
        PC_MX           : out    vl_logic
    );
end Zuse2;
