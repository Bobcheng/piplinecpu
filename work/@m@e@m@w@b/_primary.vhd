library verilog;
use verilog.vl_types.all;
entity MEMWB is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        afterMUX        : in     vl_logic_vector(4 downto 0);
        ALUout          : in     vl_logic_vector(31 downto 0);
        DMout           : in     vl_logic_vector(31 downto 0);
        WBctrl          : in     vl_logic_vector(1 downto 0);
        afterMUX_out    : out    vl_logic_vector(4 downto 0);
        ALUout_out      : out    vl_logic_vector(31 downto 0);
        DMout_out       : out    vl_logic_vector(31 downto 0);
        Men2Reg         : out    vl_logic;
        RegWr           : out    vl_logic
    );
end MEMWB;
