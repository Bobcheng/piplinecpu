library verilog;
use verilog.vl_types.all;
entity ctrlcmd is
    port(
        OpCode          : in     vl_logic_vector(5 downto 0);
        funct           : in     vl_logic_vector(5 downto 0);
        ExtOp           : out    vl_logic_vector(1 downto 0);
        ctrl_out        : out    vl_logic_vector(12 downto 0)
    );
end ctrlcmd;
