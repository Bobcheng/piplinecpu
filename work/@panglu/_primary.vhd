library verilog;
use verilog.vl_types.all;
entity Panglu is
    port(
        EXMEM_RegWr     : in     vl_logic;
        MEMWB_RegWr     : in     vl_logic;
        EXMEM_afterMUX  : in     vl_logic_vector(4 downto 0);
        MEMWB_afterMUX  : in     vl_logic_vector(4 downto 0);
        IDEX_PRt        : in     vl_logic_vector(4 downto 0);
        IDEX_PRs        : in     vl_logic_vector(4 downto 0);
        ALUMUX_A        : out    vl_logic_vector(1 downto 0);
        ALUMUX_B        : out    vl_logic_vector(1 downto 0)
    );
end Panglu;
