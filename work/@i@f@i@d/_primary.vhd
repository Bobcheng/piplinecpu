library verilog;
use verilog.vl_types.all;
entity IFID is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        IFIDWr          : in     vl_logic;
        IFID_clr        : in     vl_logic;
        PC_in           : in     vl_logic_vector(31 downto 0);
        instr_in        : in     vl_logic_vector(31 downto 0);
        PC_out          : out    vl_logic_vector(31 downto 0);
        instr_out       : out    vl_logic_vector(31 downto 0)
    );
end IFID;
