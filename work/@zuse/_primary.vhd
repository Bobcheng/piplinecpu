library verilog;
use verilog.vl_types.all;
entity Zuse is
    port(
        clk             : in     vl_logic;
        IDEX_MemRe      : in     vl_logic;
        IDEX_Rt         : in     vl_logic_vector(4 downto 0);
        IFID_Rt         : in     vl_logic_vector(4 downto 0);
        IFID_Rs         : in     vl_logic_vector(4 downto 0);
        Bubble          : in     vl_logic;
        ZeroMUX_out     : out    vl_logic;
        IFID_Wr         : out    vl_logic;
        PC_MX           : out    vl_logic;
        IFID_clr        : out    vl_logic
    );
end Zuse;
