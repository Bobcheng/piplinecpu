library verilog;
use verilog.vl_types.all;
entity IDEX is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        MuRd            : in     vl_logic_vector(4 downto 0);
        MuRt            : in     vl_logic_vector(4 downto 0);
        PRt             : in     vl_logic_vector(4 downto 0);
        PRs             : in     vl_logic_vector(4 downto 0);
        signEXT         : in     vl_logic_vector(31 downto 0);
        imre2           : in     vl_logic_vector(31 downto 0);
        imre1           : in     vl_logic_vector(31 downto 0);
        PC              : in     vl_logic_vector(31 downto 0);
        EXctrl          : in     vl_logic_vector(6 downto 0);
        Mctrl           : in     vl_logic_vector(2 downto 0);
        WBctrl          : in     vl_logic_vector(1 downto 0);
        jump            : in     vl_logic;
        MuRd_out        : out    vl_logic_vector(4 downto 0);
        MuRt_out        : out    vl_logic_vector(4 downto 0);
        PRt_out         : out    vl_logic_vector(4 downto 0);
        PRs_out         : out    vl_logic_vector(4 downto 0);
        signEXT_out     : out    vl_logic_vector(31 downto 0);
        imre2_out       : out    vl_logic_vector(31 downto 0);
        imre1_out       : out    vl_logic_vector(31 downto 0);
        PC_out          : out    vl_logic_vector(31 downto 0);
        ALUsrc_out      : out    vl_logic;
        ALUop_out       : out    vl_logic_vector(4 downto 0);
        RegDst_out      : out    vl_logic;
        Mctrl_out       : out    vl_logic_vector(2 downto 0);
        WBctrl_out      : out    vl_logic_vector(1 downto 0);
        jump_out        : out    vl_logic
    );
end IDEX;
