library verilog;
use verilog.vl_types.all;
entity EXMEM is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        afterMUX        : in     vl_logic_vector(4 downto 0);
        ALUbUp          : in     vl_logic_vector(31 downto 0);
        ALUout          : in     vl_logic_vector(31 downto 0);
        BranchPC        : in     vl_logic_vector(31 downto 0);
        Mctrl           : in     vl_logic_vector(2 downto 0);
        WBctrl          : in     vl_logic_vector(1 downto 0);
        afterMUX_out    : out    vl_logic_vector(4 downto 0);
        ALUbUp_out      : out    vl_logic_vector(31 downto 0);
        ALUout_out      : out    vl_logic_vector(31 downto 0);
        MemWr           : out    vl_logic;
        MemRe           : out    vl_logic;
        WBctrl_out      : out    vl_logic_vector(1 downto 0)
    );
end EXMEM;
