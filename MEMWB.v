


module MEMWB (clk, rst, afterMUX, ALUout, DMout, WBctrl,
              afterMUX_out, ALUout_out, DMout_out, Men2Reg, RegWr);
              
   input         clk;
   input         rst;
   input [4:0]afterMUX;
   input [31:0] ALUout;
   input[31:0] DMout;
   input [1:0] WBctrl;
   
   output reg[4:0] afterMUX_out;
   output reg [31:0] ALUout_out;
   output reg[31:0] DMout_out;
   output reg Men2Reg;
   output reg RegWr;

   reg[80:0] alldata;
               
   always @(posedge clk or posedge rst) 
   begin
      alldata = {6'd0,WBctrl,DMout,ALUout,3'd0,afterMUX};
      if ( rst ) 
         alldata <= 0;
      afterMUX_out = alldata[4:0];
      ALUout_out = alldata[39:8];
      DMout_out = alldata[71:40];
      Men2Reg = alldata[72];
      RegWr = alldata[73];      
   end // end always
      
endmodule

//???????




