module im_4k( addr, dout );
    
    input [4:0] addr;
    output [31:0] dout;
    
    reg [31:0] Dout;
    reg [31:0] imem[1023:0];
    
   	always@(addr)
	  begin
		  Dout = imem[addr];	
	  end
    assign dout = Dout;
    
endmodule    
