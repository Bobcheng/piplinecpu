module IDEX (clk, rst, MuRd, MuRt, PRt, PRs, signEXT, imre2, imre1, PC, EXctrl, Mctrl, WBctrl,jump,
              MuRd_out, MuRt_out,  PRt_out, PRs_out, signEXT_out, imre2_out, imre1_out, PC_out,
               ALUsrc_out, ALUop_out, RegDst_out, Mctrl_out, WBctrl_out, jump_out);
              
   input         clk;
   input         rst;
   input      [4:0] MuRd;
   input    [4:0] MuRt;
   input    [4:0] PRt;
   input    [4:0] PRs;
   input    [31:0] signEXT;
   input    [31:0] imre2;
   input    [31:0] imre1;
   input    [31:0] PC;
   input    [6:0] EXctrl;
   input    [2:0] Mctrl;
   input    [1:0] WBctrl;
   input    jump;
   
   output  reg[4:0] MuRd_out;
   output  reg[4:0] MuRt_out;
   output  reg[4:0] PRt_out;
   output  reg[4:0] PRs_out;
   output  reg[31:0] signEXT_out;
   output  reg[31:0] imre2_out;
   output  reg[31:0] imre1_out;
   output  reg[31:0] PC_out;
   output  reg ALUsrc_out;
   output  reg [4:0]ALUop_out;
   output  reg RegDst_out;

   output  reg [2:0] Mctrl_out;
   output  reg [1:0] WBctrl_out;
   output reg jump_out;
    
   //reg[31:0]  instr;
   //reg[31:0]  PC;
   
   reg[180:0] alldata;
               
   always @(posedge clk or posedge rst) 
   begin
      alldata = {5'd0,jump,WBctrl,1'd0,Mctrl,1'd0,EXctrl,PC,imre1,imre2,signEXT,3'd0,PRs,3'd0,PRt,3'd0,MuRt,3'd0,MuRd};
      if ( rst ) 
         alldata <= 0;
      MuRd_out = alldata[4:0];
      MuRt_out = alldata[12:8];
      PRt_out = alldata[20:16];
      PRs_out  = alldata[28:24];
      signEXT_out = alldata[63:32];
      imre2_out =  alldata[95:64];
      imre1_out = alldata[127:96];
      PC_out = alldata[159:128];
      ALUsrc_out = alldata[160];
      ALUop_out = alldata[165:161];
      RegDst_out = alldata[166];
      Mctrl_out = alldata[170:168];
      WBctrl_out = alldata[173:172];
      jump_out = alldata[174];
      
   end // end always
      
endmodule

//???????

