module mips( clk, rst );
   input   clk;
   input   rst;
   
//   wire 		     RFWr;
//   wire 		     DMWr;
//   wire 		     PCWr;
//   wire 		     IRWr;
//   wire [1:0]  EXTOp;
//   wire [1:0]  ALUOp;
//   wire [1:0]  NPCOp;
//   wire 		     BSel;
//   wire 		     Zero;
      
   //IM	
	wire [4:0]  imAdr;
	wire [31:0] opCode;
	//PC	
	wire [31:0] pcOut;
	//Alu
	wire [31:0] aluDataIn2;
	wire [31:0]	aluDataOut;
	wire 		zero;
	//Ctrl
	wire [5:0]		op;
	wire [5:0]		funct;
	wire 		jump;						//????
	wire 		RegDst;						
	wire 		Branch;						//??
	wire 		MemR;						//????
	wire 		Mem2R;						//??????????
	wire 		MemW;						//??????
	wire 		RegW;						//????????
	wire		Alusrc;						//????????
	wire [1:0]		ExtOp;						//???/??????
	wire [4:0]  AluOp;						//Alu????
	//Extender
	wire [15:0] extDataIn;
	wire [31:0] extDataOut;
	//GPR
	wire [4:0] gprWeSel,gprReSel1,gprReSel2;
	wire [31:0] gprDataIn;
	wire [31:0] gprDataOut1,gprDataOut2;
	//DMem
	wire [4:0]  dmDataAdr;
	wire [31:0] dmDataOut;
	wire [31:0] jumpaddr ;
	wire [31:0] pc_in;
   
  assign pcSel = ((Branch&&zero)==1)?1:0;
  
   PC U_PC (
      .clk(clk), .rst(rst), .PCWr(pcSel), .J(jump), .MX(??), .NPC(pc_in), .PC(pcOut)
   ); 
   assign imAdr = pcOut[6:2];
   
   im_4k U_IM ( 
      .addr(imAdr) , .dout(opCode)
   );
   assign op = opCode[31:26];
	assign funct = opCode[5:0];
	assign gprReSel1 = opCode[25:21];
	assign gprReSel2 = opCode[20:16];
	assign gprWeSel = (RegDst==0)?opCode[20:16]:opCode[15:11];
	assign extDataIn = opCode[15:0];
	assign jumpaddr[25:0] = opCode[25:0];
    
   RF U_RF (
      .A1(gprReSel1), .A2(gprReSel2), .A3(gprWeSel), .WD(gprDataIn), .clk(clk), 
      .RFWr(RegW), .RD1(gprDataOut1), .RD2(gprDataOut2)
   );
   
   ctrlcmd U_CTRL (
      .jump(jump), .RegDst(RegDst),.Branch(Branch),.MemR(MemR),.Mem2R(Mem2R)
				,.MemW(MemW),.RegW(RegW),.Alusrc(Alusrc),.ExtOp(ExtOp),.AluOp(AluOp)
				,.OpCode(op),.funct(funct)
   );
   EXT U_EXT (.Imm16(extDataIn), .EXTOp(ExtOp), .Imm32(extDataOut));
   assign aluDataIn2 = (Alusrc==1)?extDataOut:gprDataOut2;
   assign pc_in = (jump == 1)?jumpaddr:extDataOut;
   
   alu U_alu(.A(gprDataOut1), .B(aluDataIn2), 
      .ALUOp(AluOp), .C(aluDataOut), .Zero(zero)
   );
   
   assign gprDataIn = (Mem2R==0)?dmDataOut:aluDataOut;
   
   assign dmDataAdr = aluDataOut[4:0];
   
   dm_4k U_dm( .addr(dmDataAdr), .din(gprDataOut2), .DMWr(MemW), 
      .clk(clk), .dout(dmDataOut) );
   
   
  
endmodule