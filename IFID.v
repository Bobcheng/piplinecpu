module IFID (clk, rst, IFIDWr, IFID_clr, PC_in, instr_in, PC_out, instr_out);
               
   input         clk;
   input         rst;
   input         IFIDWr; 
   input        IFID_clr;
   input   [31:0]  PC_in;
   input   [31:0]  instr_in;
   output    reg[31:0]   PC_out;
   output   reg[31:0]    instr_out;
   //input  [63:0] IFID_in; //PC--height?ins--lower
   //reg[31:0]  instr;
   //reg[31:0]  PC;
   
   reg [63:0]alldata;
               
   always @(posedge clk or posedge rst) 
   begin
      if ( rst || IFID_clr) 
       begin
         PC_out <= 32'd0;
         instr_out <= 32'd0;
       end
      else if (IFIDWr)
        alldata = {instr_in, PC_in};
     instr_out = alldata[63:32];
     PC_out = alldata[31:0];

   end // end always
      
endmodule

//???????
