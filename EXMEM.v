

module EXMEM (clk, rst, afterMUX, ALUbUp, ALUout, BranchPC, Mctrl, WBctrl,
              afterMUX_out, ALUbUp_out, ALUout_out,  MemWr, MemRe, WBctrl_out);
              
   input         clk;
   input         rst;
   input [4:0]afterMUX;
   input [31:0] ALUbUp;
   input [31:0] ALUout;
   input [31:0]BranchPC;
   input [2:0]Mctrl;
   input [1:0] WBctrl;
   
   output reg[4:0]afterMUX_out;
   output reg [31:0] ALUbUp_out;
   output reg [31:0] ALUout_out;
   //output reg [31:0]BranchPC_out;
   output reg MemWr;
   output reg MemRe;
   //output reg Branch;
   output reg [1:0] WBctrl_out;
  
    
   //reg[31:0]  instr;
   //reg[31:0]  PC;
   
   reg[120:0] alldata;
               
   always @(posedge clk or posedge rst) 
   begin
      alldata = { 10'd0,WBctrl,1'd0,Mctrl,BranchPC,ALUout ,ALUbUp,3'd0,afterMUX};
      if ( rst ) 
         alldata <= 0;
      afterMUX_out = alldata[4:0];
      ALUbUp_out = alldata[39:8];
      ALUout_out = alldata[71:40];
      //BranchPC_out = alldata[103:72];
      MemWr = alldata[104];
      MemRe = alldata[105];
     // Branch = alldata[106];
      WBctrl_out = alldata[109:108];
      
   end // end always
      
endmodule

//???????



