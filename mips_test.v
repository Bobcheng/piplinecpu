`include "instruction_def.v"
`include "ctrl_encode_def.v"
module mips_test( clk, rst );
   input   clk;
   input   rst;

   //PC
   //wire PCWr;
   wire jump;
   wire MX;
   wire [31:0]NPC;
   wire [31:0]PCout;

   //IM
   wire [31:0]IF_dout;
   wire [4:0] imadr;
//ID
   wire IFID_clr;
   wire branch;
   wire branch_out;
   wire bubble;
   wire origin_bubble;
   wire [31:0]ID_PC;
   wire [31:0]ID_inst;
   reg [31:0]branch_addr;
   wire [31:0]jump_addr;
   reg [31:0]branch_temp;
   //ctrl
   wire [1:0]ExtOp;
   wire [12:0]ID_ctrl_out;
   wire [12:0]ID_ctrl_out2;
   //EXT
   wire [31:0]ID_imm32;
   //RF
   wire [31:0]ID_RD1;
   wire [31:0]ID_RD2;
   //zuse
   wire ID_IFIDWr;
   //zeromux_s
   wire ID_zeroMUX_S;
//EX
   wire  [31:0]EX_PCout;
   wire  [4:0]EX_MuRd_out;
   wire  [4:0]EX_MuRt_out;
   wire  [4:0]EX_PRt_out;
   wire  [4:0]EX_PRs_out;
   wire  [31:0]EX_signEXT_out;
   wire  [31:0]EX_imre2_out;
   wire  [31:0]EX_imre1_out;
   wire  EX_RegDst_out;
   wire  EX_ALUsrc_out;
   wire  [2:0]EX_Mctrl_between;
   wire  [1:0]EX_WBctrl_between;
   wire EX_jump_out;
   //alu
   wire  [4:0]EX_aluop;
   wire  [31:0]EX_alu_A;
   wire  [31:0]EX_alu_B;
   wire  [31:0]EX_MUXB1_B2;
   wire  [31:0]EX_alu_out;
   wire  alu_zero;
   //bottomMUX
   wire  [4:0]bottomMUX_out;
   //panglu
   wire  [1:0]EX_panglu_MU1;
   wire  [1:0]EX_panglu_MU2;

//MEM

   wire  [4:0]MEM_afterMUX_btw;
   wire  [1:0]MEM_Wctrl_btw;
   //DM
   wire  [31:0]MEM_DMaddr;
   wire  [31:0]MEM_DMdata;
   wire  MEM_DMWri;
   wire  [31:0]MEM_DMout;
   wire  [4:0]dmDataAdr;

//WB
   wire  [31:0]WB_DMout;
   wire  [4:0]WB_afterMUX_out;
   wire  [31:0]WB_ALUout_out;
   wire  WB_Men2Reg;
   wire  WB_RegWr;
   //WB_MUX
   wire  [31:0]WB_MUX_WD;

   reg [31:0]PC_4;
   reg zero = 0;
   reg one = 1;
   mux2 #(13) IDzero_MUX  (.d0(ID_ctrl_out), .d1(13'd0), .s(ID_zeroMUX_S), .y(ID_ctrl_out2));
   mux4  #(32) EX_ALUMUX_A(.d0(EX_imre1_out), .d1(WB_MUX_WD), .d2(MEM_DMaddr), .d3(), .s(EX_panglu_MU1), .y(EX_alu_A));
   mux4 #(32) EX_ALUMUX_B (.d0(EX_imre2_out), .d1(WB_MUX_WD), .d2(MEM_DMaddr), .d3(), .s(EX_panglu_MU2), .y(EX_MUXB1_B2));
   mux2 #(32) EX_ALUMUX_B2 (.d0(EX_MUXB1_B2), .d1(EX_signEXT_out), .s(EX_ALUsrc_out), .y(EX_alu_B));
   mux2 #(32) WB_MUX (.d0(WB_DMout), .d1(WB_ALUout_out), .s(WB_Men2Reg), .y(WB_MUX_WD));
   mux2 #(32) ID_jumpMUX (.d0(branch_addr), .d1(jump_addr), .s(jump), .y(NPC));
   mux2 #(5) EX_bottomMUX (.d0(EX_MuRt_out), .d1(EX_MuRd_out), .s(EX_RegDst_out), .y(bottomMUX_out));

   PC U_PC ( .clk(clk), .rst(rst), .PCWr(PCWr), .J(jump), .MX(MX), .NPC(NPC), .PC(PCout));
   assign imadr = PCout[6:2];
   im_4k U_im ( .addr(imadr), .dout(IF_dout));

   always@(PCout)
   begin
   	assign PC_4 = PCout+4;
   end
   

   IFID U_ifid (.clk(clk), .rst(rst), .IFIDWr(ID_IFIDWr), .IFID_clr(IFID_clr), .PC_in(PC_4), .instr_in(IF_dout),
   				.PC_out(ID_PC), .instr_out(ID_inst));
   assign jump_addr[25:0] = ID_inst[25:0];
   ctrlcmd U_ctrlcmd(.OpCode(ID_inst[31:26]),.funct(ID_inst[5:0]),
               .ExtOp(ExtOp), .ctrl_out(ID_ctrl_out));
   //如果是跳转指令就增加一个nop
   assign branch = ID_ctrl_out2[9];//ID_ctrl_out[9]? or ID_ctrl_out2[9]?
   assign branch_out = EX_Mctrl_between[2];
   assign jump = ID_ctrl_out2[12];
   assign bubble = (branch || jump || branch_out)? 1:0;
   //assign  origin_bubble = (EX_Mctrl_between[1] && ((EX_MuRt_out == ID_inst[25:21]) || (EX_MuRt_out == ID_inst[20:16])))?1:0;
   Zuse U_zuse(.clk(clk),.IDEX_MemRe(EX_Mctrl_between[1]), .IDEX_Rt(EX_MuRt_out), .IFID_Rt(ID_inst[20:16]), .IFID_Rs(ID_inst[25:21]),.Bubble(bubble),//此处识别跳转的nop,是否是这个阶段存疑。
           .ZeroMUX_out(ID_zeroMUX_S), .IFID_Wr(ID_IFIDWr), .PC_MX(MX), .IFID_clr(IFID_clr));

   //Zuse2 U_Zuse2(.Bubble(bubble), .origin_bubble(origin_bubble),
    //       .ZeroMUX_out(ID_zeroMUX_S), .IFID_Wr(ID_IFIDWr), .PC_MX(MX));

   EXT U_EXT(.Imm16(ID_inst[15:0]), .EXTOp(ExtOp), .Imm32(ID_imm32));
   //计算跳转地址
   always@(EX_signEXT_out or EX_PCout)
   begin
   	branch_temp = {EX_signEXT_out[29:0],2'd0};
      branch_addr = branch_temp + EX_PCout;
   end


   RF U_rf( .A1(ID_inst[25:21]), .A2(ID_inst[20:16]), .A3(WB_afterMUX_out), .WD(WB_MUX_WD), .clk(clk), .RFWr(WB_RegWr), .RD1(ID_RD1), .RD2(ID_RD2));

   assign  PCWr = (branch_out&&(alu_zero))? 1:0;

   IDEX U_idex(.clk(clk), .rst(rst), .MuRd(ID_inst[15:11]), .MuRt(ID_inst[20:16]), .PRt(ID_inst[20:16]), .PRs(ID_inst[25:21]), .signEXT(ID_imm32), .imre2(ID_RD2), .imre1(ID_RD1), .PC(ID_PC), 
   				.EXctrl(ID_ctrl_out2[6:0]), .Mctrl(ID_ctrl_out2[9:7]), .WBctrl(ID_ctrl_out2[11:10]),.jump(ID_ctrl_out2[12]),
              .MuRd_out(EX_MuRd_out), .MuRt_out(EX_MuRt_out),  .PRt_out(EX_PRt_out), .PRs_out(EX_PRs_out), .signEXT_out(EX_signEXT_out), .imre2_out(EX_imre2_out), .imre1_out(EX_imre1_out), .PC_out(EX_PCout),
               .ALUsrc_out(EX_ALUsrc_out), .ALUop_out(EX_aluop), .RegDst_out(EX_RegDst_out), .Mctrl_out(EX_Mctrl_between), .WBctrl_out(EX_WBctrl_between), .jump_out(EX_jump_out));

   alu U_alu(.A(EX_alu_A), .B(EX_alu_B), .ALUOp(EX_aluop), .C(EX_alu_out), .Zero(alu_zero));
  
   Panglu U_panglu(.EXMEM_RegWr(MEM_Wctrl_btw[1]), .MEMWB_RegWr(WB_RegWr), .EXMEM_afterMUX(MEM_afterMUX_btw), .MEMWB_afterMUX(WB_afterMUX_out), .IDEX_PRt(EX_PRt_out), .IDEX_PRs(EX_PRs_out),
                .ALUMUX_A(EX_panglu_MU1), .ALUMUX_B(EX_panglu_MU2));

   EXMEM U_exmem(.clk(clk), .rst(rst), .afterMUX(bottomMUX_out), .ALUbUp(EX_MUXB1_B2), .ALUout(EX_alu_out), .BranchPC(), .Mctrl(EX_Mctrl_between), .WBctrl(EX_WBctrl_between),
              .afterMUX_out(MEM_afterMUX_btw), .ALUbUp_out(MEM_DMdata), .ALUout_out(MEM_DMaddr), .MemWr(MEM_DMWri), .MemRe(), .WBctrl_out(MEM_Wctrl_btw));

   assign dmDataAdr = MEM_DMaddr[4:0];
   dm_4k U_dm( .addr(dmDataAdr), .din(MEM_DMdata), .DMWr(MEM_DMWri), .clk(clk), .dout(MEM_DMout));

   MEMWB U_memwb(.clk(clk), .rst(rst), .afterMUX(MEM_afterMUX_btw), .ALUout(MEM_DMaddr), .DMout(MEM_DMout), .WBctrl(MEM_Wctrl_btw),
              .afterMUX_out(WB_afterMUX_out), .ALUout_out(WB_ALUout_out), .DMout_out(WB_DMout), .Men2Reg(WB_Men2Reg), .RegWr(WB_RegWr));


endmodule
